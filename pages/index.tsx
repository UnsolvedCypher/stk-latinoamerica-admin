import { useSession, getSession } from 'next-auth/client';
import React from 'react';
import Sqlite3 from 'better-sqlite3';
import { Container, Heading } from '@chakra-ui/layout';
import {
  Table, TableCaption, Tbody, Td, Th, Thead, Tr,
} from '@chakra-ui/table';
import Layout from '../components/layout';

interface ResponsesProps {
  info: any[];
}

export default function Page(props: ResponsesProps): React.ReactElement {
  return (
    <Layout>
      <Container>
        <br />
        <br />
        <Heading>Respuestas</Heading>
        <Table variant="simple">
          <Thead>
            <Tr>
              <Th>Campeonato</Th>
              <Th>Usuario</Th>
              <Th>Comentarios</Th>
              <Th>Fecha</Th>
            </Tr>
          </Thead>
          <Tbody>
            {props.info.sort((a, b) => (new Date(a).getTime() < new Date(b).getTime() ? -1 : 1))
              .map((r) => (
                <Tr>
                  <Td>{r.campeonato}</Td>
                  <Td>{r.username}</Td>
                  <Td>{r.comentarios}</Td>
                  <Td>{r.fecha}</Td>
                </Tr>
              ))}
          </Tbody>
        </Table>
      </Container>
    </Layout>
  );
}

// Export the `session` prop to use sessions with Server Side Rendering
export async function getServerSideProps(context) {
  const session = await getSession(context);
  if (!session || !session.user) {
    return {
      redirect: {
        destination: '/api/auth/signin',
        permanent: false,
      },
    };
  } if (session.user.name !== 'UnsolvedCypher' && session.user.name !== 'G.Kart') {
    return {
      redirect: {
        destination: '/api/auth/signout',
        permanent: false,
      },
    };
  }

  const db = new Sqlite3(`/home/${process.env.USER}/respuestas.db`);

  const info = db.prepare('SELECT * FROM respuesta')
    .all()
    .map((r) => JSON.parse(r.respuesta));

  return {
    props: {
      info,
    },
  };
}
