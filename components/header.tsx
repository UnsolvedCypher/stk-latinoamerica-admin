import Link from 'next/link';
import { signIn, signOut, useSession } from 'next-auth/client';
import React from 'react';
import styles from './header.module.css';

// The approach used in this component shows how to built a sign in and sign out
// component that works on pages which support both client and server side
// rendering, and avoids any flash incorrect content on initial page load.
export default function Header(): React.ReactElement {
  const [session, loading] = useSession();

  return (
    <header>
      <noscript>
        <style>{'.nojs-show { opacity: 1; top: 0; }'}</style>
      </noscript>
      <div className={styles.signedInStatus}>
        <p className={`nojs-show ${(!session && loading) ? styles.loading : styles.loaded}`}>
          {!session && (
          <>
            <span className={styles.notSignedInText}>No iniciaste sessión</span>
            <a
              href="/api/auth/signin"
              className={styles.buttonPrimary}
              onClick={(e) => {
                e.preventDefault();
                signIn();
              }}
            >
              Iniciar sessión
            </a>
          </>
          )}
          {session && (
          <>
            {session.user.image && <span style={{ backgroundImage: `url(${session.user.image})` }} className={styles.avatar} />}
            <span className={styles.signedInText}>
              <small>Usuario:</small>
              <br />
              <strong>{session.user.email || session.user.name}</strong>
            </span>
            <a
              href="/api/auth/signout"
              className={styles.button}
              onClick={(e) => {
                e.preventDefault();
                signOut();
              }}
            >
              Salir
            </a>
          </>
          )}
        </p>
      </div>
    </header>
  );
}
